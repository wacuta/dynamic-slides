import firebase from 'firebase/app'
import "firebase/storage"
import "firebase/firestore"

const ENV = import.meta.env

// config firebase
firebase.initializeApp({
    apiKey: ENV.VITE_APIKEY,
    authDomain: ENV.VITE_AUTHDOMAIN,
    databaseURL: ENV.VITE_DATABASEURL,
    projectId: ENV.VITE_PROJECTID,
    storageBucket: ENV.VITE_STORAGEBUCKET,
    messagingSenderId: ENV.VITE_MESSAGINGSENDERID,
    appId: ENV.VITE_APPID
});

const database = firebase.firestore()
const dbPhotos = database.collection('wedding')

function getPhotos() {
    return dbPhotos
}

function uploadPhoto(f) {
    
    let name = 'photo_' + Date.now()

    // Create a root reference
    var storageRef = firebase.storage().ref(name);

    storageRef.put(f).then((snapshot) => {
        snapshot.ref.getDownloadURL().then(function(downloadURL) {
            addPhoto(name, downloadURL)
        });
        console.log('Uploaded a blob or file!')
    });
}

function addPhoto(name, url) {
    // TODO
    return getPhotos().doc(name).set({
        date: firebase.firestore.FieldValue.serverTimestamp(),
        url: url
    }).then(() => {
        console.log("Photo successfully add!");
    })
}

export {
    getPhotos,
    uploadPhoto
}
