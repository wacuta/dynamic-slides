import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import App from '@/App.vue'
import Home from '@/views/Home.vue'
import Post from '@/views/Post.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', name: 'Home', component: Post},
        {path: '/slide', name: 'Award', component: Home}
    ]
})

createApp(App)
    .use(router)
    .mount('#app')
