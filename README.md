# Dynamic Slides

Petite application de diapo en ligne avec ajout de photo dynamique.  
> L'application tourne avec le framework [VueJs 3](https://v3.vuejs.org/) feat. [ViteJs](https://vitejs.dev/) avec un stockage dynamique [Firebase Database](https://firebase.google.com/docs/database/web/read-and-write).

Le principe est d'ajouter des photos via l'url `/` et de voir les photos défiler en continue sur `/slide`.  

Tout est stocké et synchronisé sur [Firebase Database](https://firebase.google.com/docs/database/web/read-and-write).  

## Lance le projet

> installer les dépendance la première fois avec `npm i`

```bash
npm run dev
```

## Mise en prod

```bash
docker-compose -f docker-compose.prod.yml up --build
```
